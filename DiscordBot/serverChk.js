const net = require('net');  					 // https://nodejs.org/api/net.html for reference
const dUtils  = require("./discordUtils.js");

function CheckServerStatus(name, adress, port, timeout, channel)
{
	// proudly copypasted and modified from "https://www.hacksparrow.com/a-port-scanner-in-node-js.html"
	// the port scanning loop 
	//var iteration = 0;
	//var msgs = ["Game server" , "Login server"];
	//while (port <= endport) 
	//{
	var srvMsg = "";
	var say = false;
	if(typeof name != 'undefined')
	{
		srvMsg = name;
	}
	if(typeof channel != 'undefined')
	{
		say = true;
	}
	
	if(name && adress  &&  port  &&  timeout)
	{
		if(typeof adress!='undefined' &&  typeof port!='undefined' && typeof timeout!='undefined')
		{
			
			// the reason we encapsulate the socket creation process is because we want to preseve the value of `port` for the callbacks 
			(function(port) 
				{
					var s = new net.Socket();
					
					s.setTimeout(timeout, function() 
					{ s.destroy(); });
					s.connect(port, adress, function() 
					{
						console.log('OPEN: ' + port);
					});
					
					s.on('connect', function(data) 
					{
						console.log("Server Check : connection opened to target port " + port +': data '+ data);
						srvMsg = srvMsg + " Online";
						if(say)
						{
							dUtils.SayInChannel(channel, srvMsg);
						}
						s.destroy();
					});
					
					s.on('error', function(e) 
					{
						console.log("Server Check : error(unknown) connecting to " + adress + " port " + port);
						srvMsg = srvMsg + " Unreachable";
						if(say)
						{
							dUtils.SayInChannel(channel, srvMsg);
						}
						s.destroy();
					});
					
					s.on('timeout', function(to) 
					{
						console.log("Server Check : error(timeout) connecting to " + adress + " port " + port);
						srvMsg = srvMsg + " Offline";
						if(say)
						{
							dUtils.SayInChannel(channel, srvMsg);
						}
						s.destroy();
					});
			})
			
			(port);
		}
		else
		{
			console.log("some stuff have been sent undefined at CheckServerStatus@serverChk.js");
		}
	}
	else
	{
		console.log("undefined values sent to server checker, abort execution");
		
	}

}

module.exports.CheckServerStatus    = CheckServerStatus;