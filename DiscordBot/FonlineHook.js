const net = require('net');  					 // https://nodejs.org/api/net.html for reference
const dUtils  = require("./discordUtils.js");
const Discord  = require('discord.js'); 		// discord api lib include
const config   = require("./botcfg.json");	    // config file include
const defines  = require("./defines.js"); 	    // defines include
const objects  = require("./botObjects.js"); 	// objects classes for bot module

var server;
var client;
var HookSocket;
var isAuthorized = false;
var Initialized = false;

var allowedchannels = [];

function InitializeFonlineHook(bot)
{
	client = bot;
	server = net.createServer
	(
		function(socket) 
		{
			HookSocket = socket;
			var guilds    = bot.guilds.array();
			for(var i = 0, j=guilds.length; i<j; i++)
			{
				var channels = guilds[i].channels.array();
				for(var k = 0, m = channels.length; k<m; k++)
				{
					if(channels[k].name == config.botChannel)
					{
						dUtils.SayInChannel(channels[k], "Server Connected!");
						allowedchannels.push(channels[k]);
					}
				}
			}
			
			socket.on('data', OnRecieveData);
			socket.on('error', OnError );
			
			console.log("Server Connected!");

		}
	);
	
	
	server.maxConnections = 1;

	console.log("Fonline Hook initialized");

	server.listen(config.FOnlineHookListen, config.FOnlineHookAddr);
	Initialized = true;
	
}

function OnRecieveData(data)
{
	var response;
	if(!isAuthorized)
	{
		//console.log(data);
		if(data[0] === defines.NET_HEADER_AUTH)
		{
			var token = Buffer.from(data).slice(1,data.length);
			//console.log(token);
			if(token == config.FOnlineHookToken)
			{
				isAuthorized = true;
				console.log("Server authorized!");
				this.write(CreateNetSignal(defines.NET_SIGNAL_OK));
				for(var i = 0, j = allowedchannels.length; i<j; i++)
				{
					dUtils.SayInChannel(allowedchannels[i], "Server Authorized!");
				}
			}
			else
			{
				this.write(CreateNetSignal(defines.NET_SIGNAL_NO));
			}
		}
		else
		{
			this.write(CreateNetSignal(defines.NET_SIGNAL_NO));
		}
	}
	else
	{
		//console.log(data);	
		var bufSize = data.length;
		var dataType = data[0];
		if(dataType === defines.NET_HEADER_COMMAND)
		{
			var command = data[1];
			if(command === defines.NET_CMD_SET_PLAYER_COUNT)
			{
				UpdatePlayerCount(data[2]);
			}
		}
		else
		if(dataType === defines.NET_HEADER_SIGNAL)
		{
			
		}
		
		else
		if(dataType === defines.NET_HEADER_MESSAGE)
		{
			var message = Buffer.from(data).slice(1,data.length);
			for(var i = 0, j = allowedchannels.length; i<j; i++)
			{
				dUtils.SayInChannel(allowedchannels[i], "Server Message : " + message);
			}
		}
	}
}

function SendServerCommand(commandType, arguments)
{
	if(isAuthorized)
	{
		console.log("Sending server request");
		HookSocket.write(CreateNetCommand(commandType, arguments));
		
	}
}

function OnError(error)
{
	isAuthorized = false;
	console.log("Connection have met an error " + error.message);
	for(var i = 0, j = allowedchannels.length; i<j; i++)
	{
		dUtils.SayInChannel(allowedchannels[i], "Server's gone down :(");
		client.user.setActivity(config.botDesc);
	}
}

function TestAnswer(server, text)
{
	server.write(text);
}

function UpdatePlayerCount(count)
{
	client.user.setActivity(config.botDesc + ", Players : " + count);
}

function CreateNetSignal(signalType)
{
	var signal = new Buffer(defines.NET_SIGNAL_SIZE);
	signal[0] = defines.NET_HEADER_SIGNAL;
	signal[1] = signalType;
	return signal;
}

function CreateNetCommand(commandType, arguments)
{
	var argsSizes	  = [];
	var argsCnt 	  = 0;
	var totalArgsSize = 0;
	
	if(typeof arguments != "string")
	{
		for(var i = 0, j = arguments.length; i<j; i++)
		{
			argsCnt++;
			var objSize = Object.keys(arguments[i]).length;
			if(objSize === 0)
			{
				var type = typeof arguments[i];
				switch(type)
				{
					case "number":
						objSize = 4; // its 8 for JS, but i want it to be 4 bytes, because i am a 32bit bastard
				}
			}
			
			totalArgsSize += objSize;
			argsSizes.push(objSize);
			console.log('Size of object: '+ objSize);
		}
	}
	else
	{
		totalArgsSize = arguments.length;
		argsCnt = 1;
		argsSizes[0] = totalArgsSize;
	}
	
	console.log(""+arguments);
	console.log("Args count is " + argsCnt);
	var commandBuf = new Buffer(3 + argsCnt);
	var argumentsBuf = new Buffer(totalArgsSize);
	var bufferIndex = 0;
	switch(commandType) // make serializator in future, this is bullshit code
	{
		case defines.NET_SERVER_CMD_KILL_PLAYER:
			argumentsBuf.write(arguments);
			break;
			
		case defines.NET_SERVER_CMD_RUNSCRIPT:
			argumentsBuf.write(arguments[0], bufferIndex, bufferIndex+argsSizes[0]);
			bufferIndex += argsSizes[0];
			
			argumentsBuf.write(arguments[1], bufferIndex, bufferIndex+argsSizes[1]);
			bufferIndex += argsSizes[1];
			
			argumentsBuf.writeInt32LE(arguments[2], bufferIndex);
			bufferIndex += argsSizes[2];
			
			argumentsBuf.writeInt32LE(arguments[3], bufferIndex);
			bufferIndex += argsSizes[3];
			
			argumentsBuf.writeInt32LE(arguments[4], bufferIndex);
			break;
		case defines.NET_SERVER_CMD_RUNCHEAT:
			argumentsBuf.write(arguments);
			break;
		default:
			argumentsBuf.write(arguments);
			break;
	}
	
	commandBuf[0] = defines.NET_HEADER_COMMAND;
	commandBuf[1] = commandType;
	commandBuf[2] = argsCnt;
	
	for(var i = 3, j = commandBuf.length; i<j ; i++)
	{
		commandBuf[i] = argsSizes[i-3];
	}
	
	var command = Buffer.concat([commandBuf, argumentsBuf]);
	console.log(command);
	return command;
}


function ParseCommandArgs(buffer)
{
	var arguments = [];
	for(var i = 2, j = buffer.length; i<j; i++)
	{
		arguments.push[buffer[i]];
	}
	return arguments;
}

function pack(bytes) {
    var str = "";
// You could make it faster by reading bytes.length once.
    for(var i = 0; i < bytes.length; i += 2) {
// If you're using signed bytes, you probably need to mask here.
        var char = bytes[i] << 8;
// (undefined | 0) === 0 so you can save a test here by doing
//     var char = (bytes[i] << 8) | (bytes[i + 1] & 0xff);
        if (bytes[i + 1])
            char |= bytes[i + 1];
// Instead of using string += you could push char onto an array
// and take advantage of the fact that String.fromCharCode can
// take any number of arguments to do
//     String.fromCharCode.apply(null, chars);
        str += String.fromCharCode(char);
    }
    return str;
}

function unpack(str) {
    var bytes = [];
    for(var i = 0; i < str.length; i++) {
        var char = str.charCodeAt(i);
// You can combine both these calls into one,
//    bytes.push(char >>> 8, char & 0xff);
        bytes.push(char >>> 8);
        bytes.push(char & 0xFF);
    }
    return bytes;
}


module.exports.InitializeFonlineHook    = InitializeFonlineHook;
module.exports.SendServerCommand		= SendServerCommand;
module.exports.Initialized				= Initialized;