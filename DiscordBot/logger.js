const fs 	   = require('fs');

function LogToFile(filename, log)
{
	var stream = fs.createWriteStream("./logs/"+filename+".txt", {flags: 'a'});

	stream.once('open', function(fd) 
	{
		stream.write(log+"\n");
		stream.end();
	});
	
	
}

module.exports.LogToFile = LogToFile;