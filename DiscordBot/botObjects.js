const Discord  = require('discord.js'); // discord api lib include
/* 
Hierarchy is 
Server has 	  [ commands and timeouts]
Commands have [ timeouts ]

Timeouts just timeouts

Option is not used, yet.



 */



// TIMEOUT
function Timeout(name, time)
{
	this.name  = name;
	this.state = false;
	this.time  = time;
}

Timeout.prototype.set = function(state) // setter
{
	this.state = state;
}

Timeout.prototype.flip = function() // timed switch that returns value back after some time
{
	setTimeout(SetTimeoutState, this.time, this, this.state);
	this.set(!this.state);
}


function SetTimeoutState(timeout, stateSet)
{
	timeout.set(stateSet);
}

// ARGUMENT
function Argument(value)
{
	this.value = value;
}

// OPTION

function Option(name)
{
	this.args = [];
	this.name = name;
}

Option.prototype.setName = function(name)
{
	this.name = name;
}

Option.prototype.addArg = function (arg)
{
	var argument = new Argument(arg);
	this.args.push(argument);
	return this;
}

// COMMAND

function Command(name, timeout, useMsg, timeoutMsg, argCount, alias) // constructor
{
	this.options = [];
	this.name = name;
	this.alias = alias;
	this.timeout = new Timeout("TO_"+name, timeout);
	this.useMsg = useMsg;
	this.timeoutMsg = timeoutMsg;
	this.argCount = argCount;
}

Command.prototype.changeName =  function(newName)
{
	this.name = newName;
}


Command.prototype.flipTimeoutState = function()
{
	this.timeout.flip();
}

Command.prototype.getTimeoutState = function()
{
	return this.timeout.state;
}

Command.prototype.setTimeoutTime = function (timeout, value)
{
	if(timeout && value)
	{
		this.timeout.time = value;
	}
}

Command.prototype.addOption = function (name)
{
	var opt = new Option(name);
	this.options.push(opt);
	return this.options[this.options.length-1];
}

// SERVER representation for BOT point of view
function BotServer(name, id)
{
	this.timeouts = [];
	this.commands = [];
	this.name = name;
	this.id   = id;
}

BotServer.prototype.createTimeout = function(TOName, time)
{
	var timeout = new Timeout(TOName,time);
	this.timeouts.push(timeout);
}

BotServer.prototype.findTimeoutIdx = function(name)
{
	for( var i = 0, j = this.timeouts.length; i<j; i++)
	{
		if(this.timeouts[i].name == name)
		return i;
	}
	return -1;
}

BotServer.prototype.addCommand = function (name, timeout, useMsg, timeoutMsg, argCount, stuff)
{
	newCommand = new Command(name, timeout, useMsg, timeoutMsg, argCount, stuff);
	this.commands.push(newCommand);
	return this.commands[this.commands.length-1];
}

BotServer.prototype.findCommandIdx = function(name)
{
	for ( var i = 0, j = this.commands.length; i<j; i++)
	{
		
		if(this.commands[i].name == name)
		{
			return i;
		}
	}
	return -1;
}

BotServer.prototype.setCommandName = function (command, newName)
{
	if(newName)
	{
		var idx = this.findCommandIdx(command);
		if(idx >0)
		{
			this.commands[idx].name = newName;
		}
	}
}

module.exports.Timeout = Timeout;
module.exports.Option = Option;
module.exports.BotServer = BotServer;