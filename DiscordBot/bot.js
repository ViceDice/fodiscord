const Discord  = require('discord.js'); 		// discord api lib include
const client   = new Discord.Client();  	    // instantiate client ( bot )
const date     = new Date();
const net 	   = require('net');  	

const config   = require("./botcfg.json");	    // config file include
const defines  = require("./defines.js"); 	    // defines include
const checker  = require("./serverChk.js");   	// server checker module
const dUtils   = require("./discordUtils.js");  // discord utils module 
const objects  = require("./botObjects.js"); 	// objects classes for bot module
const Fonline  = require("./FonlineHook.js");

const logger   = require("./logger.js");

client.login(config.token); 					// login the bot with its token taken from config file

var botServers = []; 							// associative array lookup thingie, guild id is equal to its idx in this array
// TODO : link SQL database for better data saving/use/sorting

//BOT EVENTS
client.on("ready", function()
{
	bot_onReady();
}
); // init event

client.on("message", function(message)
{
	bot_onMessage(message);
}
); // on message event

function tryExecuteCommand(message)
{
	const args        = message.content.slice(config.commandPrefix.length).trim().toLowerCase().split(/ +/g);
	var currentSrv    = bot_findServerById(message.guild.id);
	var command		  = bot_GetCommand(currentSrv, args[defines.COMMAND_ARGS_CMD]);
		
	var commandExec = false ; 
	
	
	if(command)
	{
		if(checkOwnerCommand(command))
		{
 			if(!IsOwner(message.author))
			{
				dUtils.SayInChannel(message.channel, "This command is for owner users only");
				return;
			}
		}
		
 		if(args.length-1 === command.argCount && !command.timeout.state)   	 // -1 cuz we store the command itself in there aswell ( lazy monkey code soz)
		{
			command.flipTimeoutState();										 // flip the timeout switch, it goes back after time prop value
			
			if(command.alias == config.keyCmdServer)						 // check which command this is
			{
				if(args[defines.COMMAND_ARGS_FIRST] == "localhost" || args[defines.COMMAND_ARGS_FIRST] == "127.0.0.1")
				{
					dUtils.SayInChannel(message.channel, "Nope");
					return;
				}
				
				dUtils.SayInChannel(message.channel, config.msgCmdChkSrv + args[defines.COMMAND_ARGS_FIRST] + " : " + args[defines.COMMAND_ARGS_SECOND]);
				
				checker.CheckServerStatus("Target is", args[defines.COMMAND_ARGS_FIRST], args[defines.COMMAND_ARGS_SECOND], config.checkSrvTimeout, message.channel);
				
			}
			else
			
			if(command.alias == config.keyCmdOwners)
			{
				Owners(message.channel);
			}
			else
			
			if(command.alias == config.keyCmdInfo)
			{
				Info(message.channel);
			}
			else
			
			if(command.alias == config.keyCmdRandom)
			{
				SayRandom(message.channel, args[defines.COMMAND_ARGS_FIRST]);
			}
			else
			
			if(command.alias == config.keyCmdHelp)
			{
				Help(message);
			}
			else
			
			if(command.alias == "cmd")
			{
				var targetCmd = bot_GetCommand(currentSrv, args[defines.COMMAND_ARGS_FIRST]);
				if(targetCmd)
				{
					targetCmd.changeName(args[defines.COMMAND_ARGS_SECOND]);
					console.log("changed target cmd alias to " + targetCmd.name);
					
				}
				else
				{
					dUtils.SayInChannel(message.channel, "target command not found");
					
				}
				
			}
			else
			
			if(command.alias == config.keyClearChannel)
			{
				
			//	message.channel.bulkDelete(100, true);
				
				message.channel.fetchMessages({ limit: 100 })
				.then(messages => 
				{
					messages.deleteAll();
				})
				.catch(console.log("error"));
			}
			
			commandExec = true;
		
		}
		else
		if(args.length-1 !== command.argCount)
		{
			dUtils.SayInChannel(message.channel, command.useMsg);
		}
		else
		if(command.timeout.state)
		{
			dUtils.SayInChannel(message.channel, command.timeoutMsg);
			console.log(command.timeout.time + " " + command.timeout.state);
		}
		
		
		if(config.logging == true && config.logCmd == true && commandExec)
		{
			CmdExecLog(message.author, message.channel, command.name);	
		}
	}
	else
	{
		dUtils.SayInChannel(message.channel, "command not found");
	}

}

function tryExecuteFonlineCommand(message)
{
	const args        = message.content.slice(config.commandPrefix.length).trim().split(/ +/g); // no need to lower case all of them
	var command 	  = args[defines.COMMAND_ARGS_CMD];
	var splitArgs	  = [];
	
	if(command!="runcheat")
	{
		for(var i = 1, j = args.length; i<j; i++)
		{
			splitArgs.push(args[i]);
		}
	}
	switch(command)
	{
		case "kill":
			if(args.length == 2)
				Fonline.SendServerCommand(defines.NET_SERVER_CMD_KILL_PLAYER, args[1]);
			break;
		case "runscript":
			if(splitArgs.length == 5)
			{
				for(var i = 2, j = splitArgs.length; i<j; i++)
				{
					splitArgs[i] = parseInt(splitArgs[i], 10);
				}
				Fonline.SendServerCommand(defines.NET_SERVER_CMD_RUNSCRIPT, splitArgs);
			}
			break;
		case "runcheat": // hehehe
			var stringCheat = "";
			for(var i = 1; i<args.length-1; i++)
			{
				stringCheat += args[i] + " ";
			}
			stringCheat += args[args.length-1];
			
			Fonline.SendServerCommand(defines.NET_SERVER_CMD_RUNCHEAT, stringCheat);
		default :
			break;
	}
	
}


// bot features
function MessageLog(message)
{
	console.log(config.loggingPrefix + message.author.username + " : " + message.content);
	logger.LogToFile(message.channel.name, config.loggingPrefix + " " + date.getFullYear() + "." + date.getMonth() + "." + date.getDate() + ". " 
					+ date.getHours() + ":" + date.getMinutes() + " " +  message.author.username + " : " + message.content);
}

function CmdExecLog(user, channel, command)
{
	console.log(config.logCmdPrefix  + command + "@" +  channel.guild.name + " : " + channel.name + " for " + user.username);// + " on server " + message.server.name);
}

function BotLog(message)
{
	console.log(config.botLogPrefix + message);
}

function SayRandom(channel, upto)
{
	dUtils.SayInChannel(channel, (Math.floor(Math.random() * upto )) );  
}

function Help(message)
{
	var currentSrv    = bot_findServerById(message.guild.id);
	var commandNames  = [];
	for( var i = 0, j = currentSrv.commands.length; i<j; i++)
	{
		commandNames.push(" " + currentSrv.commands[i].name);
	}
	dUtils.SayInChannel(message.channel, "commands list \n" +commandNames);
}

function Owners(channel)
{
	var text = config.msgOwners;
	
	for( var i = 0, j=config.owners.length/2; i<j; i++)
	{
		text += ", " + config.owners[i*2];
	}
	
	dUtils.SayInChannel(channel, text);
}

function Info(channel)
{
	dUtils.SayInChannel(channel, config.msgBotInfo);
}


function IsOwner(user)
{
	for( var i = 0, j=config.owners.length/2; i<j; i++)
	{
		if(config.owners[(i*2)+1] == user.id)
		{
			return true;
		}
	}
	return false;
}

function bot_findServerById(id)     // direct id result lookup ( should be faster )
{
	return botServers[id];
}

function bot_GetCommand(srv, cmd)
{
	var cmdIdx = srv.findCommandIdx(cmd);
	
	if(cmdIdx>=0)
	{
		return srv.commands[cmdIdx];
	}
	return null;

}

function checkOwnerCommand(command)
{
	for(var i = 0, j = config.ownerCommands.length; i<j; i++)
	{
		if(command.name == config.ownerCommands[i])
			return true;
	}
	
	return false;
}



/* function bot_findServerById(id)  // id iterator lookup
{
	for(var i = 0, j = botServers.length; i<j; i++)
	{
		if(botServers[i].id == id)
		{
			return botServers[i];
		}
	}
	return null;
} */

function bot_init() // stuff we set up when the bot is connected
{
	BotLog("I am ready!" + " My servers are :")
	//console.log();
	var guilds    = client.guilds.array();		    // the initial collection is bit messy to me to operate ( dumb af )
	for(var i = 0, j=guilds.length; i<j; i++)
	{

		BotLog(guilds[i].name + " : " + guilds[i].id);
		
		var botServer = new objects.BotServer(guilds[i].name, guilds[i].id);
		bot_registerBaseCommands(botServer);
		
	
		botServers[botServer.id] = botServer; // for direct id lookup method
	}
	
	client.user.setUsername(config.botName);
	client.user.setActivity(config.botDesc);
	
 	if(config.botShuffleAvatars && config.botAvatars.length!=0)
	{
		var randomIdx = Math.floor(Math.random() * config.botAvatars.length);
		
		client.user.setAvatar(config.botAvatars[randomIdx])
		.then(user => BotLog(config.msgAvatarSet))
		.catch(user => BotLog(config.msgAvatarSetErr));
	}
	else
	{
		if(config.botAvatar !== undefined && config.botAvatar !== "")
		{
			client.user.setAvatar(config.botAvatar)
			.then(user => BotLog(config.msgAvatarSet))
			.catch(user => BotLog(config.msgAvatarSetErr));
		}
	}
	
	if(!Fonline.Initialized)
		Fonline.InitializeFonlineHook(client);
 
}

function bot_registerBaseCommands(botsrv)
{ 
//					 name,                   timeout,                    howto msg,                timeout msg,        	   args    alias
		botsrv.addCommand(config.keyCmdServer,   config.checkSrvCmdTimeout, config.msgCmdServer,  config.msgCmdTimeout,		 2, config.keyCmdServer);
		botsrv.addCommand(config.keyCmdRandom,  			  		  2000, config.msgCmdRandom,  config.msgCmdTimeout,		 1, config.keyCmdRandom);
		botsrv.addCommand(config.keyCmdHelp,   				     	  2000, config.helpMsg,		  config.msgCmdTimeout,	  	 0, config.keyCmdHelp);	
		botsrv.addCommand(config.keyCmdOwners,  				 	  2000, config.msgOwners,	  config.msgCmdTimeout,      0, config.keyCmdOwners);
		botsrv.addCommand(config.keyCmdInfo,    					  2000, config.msgBotInfo,	  config.msgCmdTimeout,		 0, config.keyCmdInfo);
		botsrv.addCommand(config.keyCmdLeave,   					  2000, config.msgLeaveGuild, config.msgCmdTimeout,		 1, config.keyCmdServer);
		botsrv.addCommand(config.keyClearChannel,   			      2000, "nan", 				  config.msgCmdTimeout,		 0, config.keyClearChannel);
		botsrv.addCommand("cmd",									  5000, "usage is cmd [target cmd], [new name]",   "timeout",  2,  "cmd");
}

//events section
function bot_onReady()
{
	bot_init();
	bot_CheckFonlineOnline();
}

function bot_onMessage(message) // event handler
{
	if (message.content.length!=0 && config.logging == true)
	{
		MessageLog(message);
	}
	
	if(!message.author.bot && message.channel.name === config.botChannel) // dont react to other bots messages and only in bot specified channel
	{	
		if (message.content.startsWith(config.commandPrefix) && (message.content.length >=4) ) 
		{
			tryExecuteCommand(message);
		}
		else
		if(message.content.startsWith(config.serverPrefix) && (message.content.length >=4))
		{
			tryExecuteFonlineCommand(message);
		}
		return;
	}
	
	if(!message.author.bot && message.channel.name === config.suggestionsChannel)
	{	
		if((Math.floor(Math.random() * 100 )) == 100)
			dUtils.SayInChannel(message.channel, "Oh my god!");
		return;
	}
}


let buff = new Buffer([0xFF, 0xFF, 0xFF, 0xFF]); // fonline specific 4 bytes to check server online

function bot_CheckFonlineOnline()
{
    var connection = new net.Socket();
    connection.setTimeout(1000);
	
    connection.connect(config.serverPort, config.serverAddr, function () 
	{
        console.log('Checking fonline server status at ' + config.serverAddr + ":" + config.serverPort);
        connection.write(buff);
    });
    
	connection.on('data', function (data) 
	{
        console.log('Received: ' + data);
        var buffer = new Buffer('', 'hex');
        buffer = Buffer.concat([buffer, new Buffer(data, 'hex')]);
        online = buffer.readUInt32LE(0);
        uptime = buffer.readUInt32LE(4);
        console.log(online);
        if (online != '') 
		{
            var uptimems = Math.round(uptime * 1000);
            var datetimenow = Date.now();
            var uptimets = Math.round(datetimenow - uptimems);
			client.user.setActivity(config.serverAlias + " Online : " + online);
        }
        connection.destroy();
    });
            
	connection.on('error', function (err) 
	{
		client.user.setActivity(config.serverAlias + " Unreachable");
        connection.destroy();
    });
    connection.on('timeout', function (err) 
	{
		client.user.setActivity(config.serverAlias + " Offline");
        connection.destroy();
    });

	setTimeout(bot_CheckFonlineOnline, 300000); // 5 minutes cd in MS
}
