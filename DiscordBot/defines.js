// JS is being interpreted, so no compiler - no actual macro definitions


const COMMAND_ARGS_CMD   = 0;
const COMMAND_ARGS_FIRST = 1;

const COMMAND_ARGS_SECOND  = 2;

const COMMAND_ARGS_THIRD = 3;

const COMMAND_ARGS_FORTH	= 4;

const ZERO = 0;
const ONE  = 1;
const TWO  = 2;
const THREE = 3;

//sizes
const NET_SIGNAL_SIZE    = 2;

//misc

const NET_HEADER_ARG_BYTE	=0;
const NET_HEADER_ARG_SHORT  =1;
const NET_HEADER_ARG_DWORD  =2;
const NET_HEADER_ARG_STRING =3;


// types
const NET_HEADER_SIGNAL  = 0;
const NET_HEADER_COMMAND = 1;
const NET_HEADER_MESSAGE = 2;
const NET_HEADER_AUTH	 = 0xFF;

// subtypes signals
const NET_SIGNAL_NO  		= 0;
const NET_SIGNAL_OK  		= 1;
const NET_SIGNAL_SHUTDOWN	= 2;
const NET_SIGNAL_START		= 3;

// subtypes commands
const NET_CMD_RESTART 				= 0;
const NET_CMD_SET_PLAYER_COUNT   	= 1;

const NET_SERVER_CMD_KILL_PLAYER	= 0;
const NET_SERVER_CMD_KICK_PLAYER    = 1;
const NET_SERVER_CMD_RUNSCRIPT		= 2;
const NET_SERVER_CMD_RUNCHEAT 		= 3;



//module.exports.COMMAND_CMD_IDX = COMMAND_CMD_IDX;
//module.exports.COMMAND_ARGS_START = COMMAND_ARGS_START;
module.exports.ZERO = ZERO;
module.exports.ONE = ONE;
module.exports.TWO = TWO;
module.exports.THREE = THREE;

module.exports.COMMAND_ARGS_CMD = COMMAND_ARGS_CMD;
module.exports.COMMAND_ARGS_FIRST = COMMAND_ARGS_FIRST;
module.exports.COMMAND_ARGS_SECOND = COMMAND_ARGS_SECOND;
module.exports.COMMAND_ARGS_THIRD = COMMAND_ARGS_THIRD;

module.exports.NET_HEADER_AUTH   = NET_HEADER_AUTH;
module.exports.NET_SIGNAL_SIZE   = NET_SIGNAL_SIZE;
module.exports.NET_HEADER_SIGNAL = NET_HEADER_SIGNAL;
module.exports.NET_SIGNAL_OK  = NET_SIGNAL_OK;
module.exports.NET_SIGNAL_NO  = NET_SIGNAL_NO;
module.exports.NET_SIGNAL_SHUTDOWN  = NET_SIGNAL_SHUTDOWN;
module.exports.NET_SIGNAL_START  = NET_SIGNAL_START;

module.exports.NET_HEADER_COMMAND	  	  = NET_HEADER_COMMAND;
module.exports.NET_CMD_RESTART		 	  = NET_CMD_RESTART;
module.exports.NET_CMD_SET_PLAYER_COUNT   = NET_CMD_SET_PLAYER_COUNT;

module.exports.NET_SERVER_CMD_KILL_PLAYER   = NET_SERVER_CMD_KILL_PLAYER;
module.exports.NET_SERVER_CMD_KICK_PLAYER   = NET_SERVER_CMD_KICK_PLAYER;
module.exports.NET_SERVER_CMD_RUNSCRIPT  	= NET_SERVER_CMD_RUNSCRIPT;
module.exports.NET_SERVER_CMD_RUNCHEAT  	= NET_SERVER_CMD_RUNCHEAT;


module.exports.NET_HEADER_MESSAGE 		  = NET_HEADER_MESSAGE;

module.exports.NET_HEADER_ARG_BYTE 		  = NET_HEADER_ARG_BYTE;
module.exports.NET_HEADER_ARG_SHORT 	  = NET_HEADER_ARG_SHORT;
module.exports.NET_HEADER_ARG_DWORD 	  = NET_HEADER_ARG_DWORD;
module.exports.NET_HEADER_ARG_STRING 	  = NET_HEADER_ARG_STRING;
