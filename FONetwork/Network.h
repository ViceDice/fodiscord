
#ifndef NETWORK_HEADER
#define NETWORK_HEADER


#define DEFAULT_BUFLEN (512)

#ifdef __FONLINE__
#define printf Log
#else
#define Log(x, ...) printf(x, __VA_ARGS__)
typedef unsigned int uint;
#endif


// STL
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <string>
#include <vector>
#include <set>
#include <map>
using namespace std;


// WINDOWS ONLY YET
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdlib.h>
#include <stdio.h>
#include <process.h>

#pragma comment (lib, "Ws2_32.lib")
#pragma comment (lib, "Mswsock.lib")
#pragma comment (lib, "AdvApi32.lib")


#define CONNECTION_STATUS_RECIEVING		(0)
#define CONNECTION_STATUS_IDLE			(1)
#define CONNECTION_STATUS_DISCONNECTED  (2)

typedef void (*RecieveProcessor)(char* recvbuf, unsigned int recvbuflen);

class Client_Connection
{
	private:
		WSAData wsaData;
		SOCKET ConnectSocket;

		struct addrinfo 
						*result,
						*ptr,
			   hints;

		std::string sendbuf;
		char* recvbuf;
		int iResult;
		unsigned int recvbufrecv;
		unsigned int recvbuflen;
		uintptr_t recieveThreadId;

		std::string address;
		std::string port;
		std::string token;
		std::string name;
		RecieveProcessor processor;
		int status;

	public:
		Client_Connection();
		~Client_Connection();
		void SetToken(std::string);
		void SetAddress(std::string);
		void SetPort(std::string);
		void SetData(std::string);
		void SetName(std::string);
		std::string GetName();
		void SetProcessor(RecieveProcessor procIn);
		void ProcessResponse();
		bool Connect(char* address, char* port);
		bool Connect();
		int	 GetStatus();
		void StartRecieve();
		void StopRecieve();
		bool Disconnect();
		bool SendData();
		void Recieve();
};

#endif