#ifndef __PROTOCOL__
#define __PROTOCOL__

// protocol command headers are like this:
// 0 byte - type
// 1 byte - subtype
// 2 byte is arg count
// 3+ bytes*arg count are args byte sizes
// everything else are actual arguments

#define NET_DISCORD_HEADER_TYPE	   (0)
#define NET_DISCORD_HEADER_SUBTYPE (1)
#define NET_DISCORD_HEADER_ARGC	   (2)
#define NET_DISCORD_HEADER_SIZES   (3)

#define NET_DISCORD_HEADER_SIGNAL  (0)
#define NET_DISCORD_HEADER_COMMAND (1)
#define NET_DISCORD_HEADER_MESSAGE (2)
#define NET_DISCORD_HEADER_AUTH	   (0xFF)

// a lot of rework required...
#define NET_DISCORD_HEADER_ARG_BYTE	  (0)
#define NET_DISCORD_HEADER_ARG_SHORT  (1)
#define NET_DISCORD_HEADER_ARG_DWORD  (2)
#define NET_DISCORD_HEADER_ARG_STRING (3)

#define NET_DISCORD_SIGNAL_NO		(0)
#define NET_DISCORD_SIGNAL_OK		(1)
#define NET_DISCORD_SIGNAL_SHUTDOWN	(2)
#define NET_DISCORD_SIGNAL_START	(3)

#define NET_DISCORD_CMD_RESTART			  (0)
#define NET_DISCORD_CMD_SET_PLAYER_COUNT  (1)

#define NET_SERVER_CMD_KILL_PLAYER		  (0)
#define NET_SERVER_CMD_KICK_PLAYER		  (1)
#define NET_SERVER_CMD_RUNSCRIPT		  (2)
#define NET_SERVER_CMD_RUNCHEAT			  (3)


#define NET_HEADER_SIGNAL  (0)
#define NET_HEADER_COMMAND (1)
#define NET_HEADER_MESSAGE (2)
#define NET_HEADER_AUTH	   (0xFF)

#define NET_SIGNAL_NO		(0)
#define NET_SIGNAL_OK		(1)
#define NET_SIGNAL_SHUTDOWN	(2)
#define NET_SIGNAL_START	(3)

#define NET_CMD_RESTART			  (0)
#define NET_CMD_SET_PLAYER_COUNT  (1)

#endif