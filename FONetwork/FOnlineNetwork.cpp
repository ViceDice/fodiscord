

#ifndef FONETWORK
#define FONETWORK
#include "FOnlineNetwork.h"

#ifdef TLS
bool InitializeDiscordHook();
void DiscordProcessResponse(char* recvbuf, uint recvbuflen);
void ProcessDiscordCommands();
void ProcessCommand(const char* command, unsigned int commandLenght);
bool Initialized = false;
bool ASBindAndPrepare(const char* scriptName, const char* funcName);
#endif


#ifdef TLS

Client_Connection* DiscordHook = NULL;
bool DiscordAuth = false;

CRITICAL_SECTION CriticalSection;  
std::vector<std::string> PendingMessages;



int InitializeNetwork()
{
	if(!Initialized)
	{
		InitializeCriticalSection(&CriticalSection);
		Initialized = InitializeDiscordHook();
		return 0;
	}
	return 0;
}

bool InitializeDiscordHook()
{

	if(DiscordHook)
	{
		DiscordHook->StopRecieve();
		DiscordHook->Disconnect();
		delete DiscordHook;
		DiscordHook = NULL;
	}

	DiscordHook = new Client_Connection();
	DiscordHook->SetProcessor(DiscordProcessResponse);
	DiscordHook->SetAddress(TEST_ADDR);
	DiscordHook->SetPort(TEST_PORT);

	std::string token = "X123456789";
	*(uint8*)(token.c_str()) = NET_DISCORD_HEADER_AUTH;

	DiscordHook->SetToken(token);
	DiscordHook->SetName("Discord_Hook\n");
	if(DiscordHook->Connect())
	{
		Log(DiscordHook->GetName().c_str());
		Log("Connected to discord bot!\n");
		DiscordHook->StartRecieve();
		return true;
	}
	else
	{
		delete DiscordHook;
		return false;
	}
}

void ShutdownNetwork()
{
	if(DiscordHook)
	{
		DiscordHook->StopRecieve();
		DiscordHook->Disconnect();
		delete DiscordHook;
		DiscordHook = NULL;
	}
}

bool SendDiscordMessage(ScriptString* sstrIn)
{
	if(DiscordHook)
	{
		
		std::string stringIn = sstrIn->c_std_str();
		uint strSize = stringIn.size();
		char * buf = new char[strSize + 1];
		buf[0] = NET_DISCORD_HEADER_MESSAGE;
		std::copy(stringIn.begin(), stringIn.end(), buf+1);
		std::string messageCommand(buf, strSize+1);
		delete[] buf;
		DiscordHook->SetData(messageCommand);

		return DiscordHook->SendData();
	}
	return false;
}

bool SendDiscordCommand(ScriptArray* command)
{
	if(DiscordHook)
	{
		uint cmdSize = command->GetSize();

		char* dataptr	 = (char*)(command->First());
		std::string commandStr(dataptr, cmdSize);

		DiscordHook->SetData(commandStr);

		return DiscordHook->SendData();

	}
	return false;
}

void DiscordProcessResponse(char* recvbuf, uint recvbufrecv)
{
	if(recvbufrecv<=0)
		return;
	else
	{
		if(!DiscordAuth)
		{
			if(recvbuf[0]==NET_DISCORD_HEADER_SIGNAL)
			{

					if(recvbuf[1] == NET_DISCORD_SIGNAL_OK)
					{
						DiscordAuth = true;
						Log("Authorized Successfully\n");
						return;
					}
					else
					if(recvbuf[1] == NET_DISCORD_SIGNAL_NO)
					{
						Log("Access denied, check your token.\n");
						return;
					}
					return;
			}
			else
			{
				return;
			}
		}
		else
		if(recvbuf[0] == NET_DISCORD_HEADER_COMMAND)
		{
			// Request ownership of the critical section. 
			EnterCriticalSection(&CriticalSection);  
 
			// Access the shared resource. 
			PendingMessages.push_back(std::string(recvbuf, recvbufrecv));
 
			// Release ownership of the critical section. 
			LeaveCriticalSection(&CriticalSection); 
			
			return;
		}
	}
}

void ProcessDiscordCommands()
{

	if(TryEnterCriticalSection(&CriticalSection))
	{
		unsigned int msgCount = PendingMessages.size();
		for(unsigned int i = 0; i<msgCount; i++)
		{
			ProcessCommand(PendingMessages[i].c_str(), PendingMessages[i].size()); // process message
			PendingMessages.erase(PendingMessages.begin() + i); // remove it

		}

		LeaveCriticalSection(&CriticalSection);
	}
}

void ProcessCommand(const char* command, unsigned int commandLenght)
{
	if(command[NET_DISCORD_HEADER_TYPE] == NET_DISCORD_HEADER_COMMAND)
	{
		uint8 cmdType			 = command[NET_DISCORD_HEADER_SUBTYPE];
		unsigned char argsCount  = command[NET_DISCORD_HEADER_ARGC];
		unsigned char* argsSizes = (unsigned char*)(&command[NET_DISCORD_HEADER_SIZES]);
		unsigned int argsOffset  = NET_DISCORD_HEADER_SIZES+argsCount;
		switch(cmdType)
		{
			case NET_SERVER_CMD_KILL_PLAYER:
				Log("Command to kill player recieved \n ");
					//if(recvbuflen == 2 + sizeof(uint)) // check the data integrity
					Command_Kill_Player( (char*)(command+argsOffset) ); // rip the headers off, only arguments
				break;
			case NET_SERVER_CMD_RUNSCRIPT:
				if(argsCount == 5)
				{
					Log("Command runscript recieved \n");
					char* arg0Ptr = (char*)(command+argsOffset);
					char* arg1Ptr = (char*)(arg0Ptr+argsSizes[0]);
					char* arg2Ptr = (char*)(arg1Ptr+argsSizes[1]);
					char* arg3Ptr = (char*)(arg2Ptr+argsSizes[2]);
					char* arg4Ptr = (char*)(arg3Ptr+argsSizes[3]);
					
 					Command_RunScript( std::string(arg0Ptr,argsSizes[0]).c_str(), std::string(NETWORK_SCRIPT_GENERIC_RUNSCRIPT).insert(5,std::string( arg1Ptr,argsSizes[1]).c_str()).c_str(), 
					*(int*)(arg2Ptr), *(int*)(arg3Ptr), *(int*)(arg4Ptr));
				}
				break;
			case NET_SERVER_CMD_RUNCHEAT:
				Command_RunCheat( (char*)(command+argsOffset) );
				break;
			default:
				break;
		}
	}
}

// todo
void SliceCommand(const char* command, unsigned int commandLength)
{

}

bool ASBindAndPrepare(const char* scriptName, const char* funcName)
{
	uint bindId = FOnline->ScriptBind(scriptName, funcName, true);
	return (bindId && FOnline->ScriptPrepare(bindId));

}

void Command_Kill_Player(char* name)
{
	ScriptString& stringName = ScriptString::Create(name);
	if(ASBindAndPrepare(NETWORK_SCRIPT_MODULE_NAME, NETWORK_SCRIPT_KILL_PLAYER))
	{
		FOnline->ScriptSetArgObject(&stringName);
		FOnline->ScriptRunPrepared();
	}

	stringName.Release(); // better than delete, i hope.
}

void Command_RunScript(const char* scriptName, const char* funcDecl, int arg0, int arg1, int arg2)
{
	if(ASBindAndPrepare(scriptName, funcDecl))
	{
		FOnline->ScriptSetArgInt(arg0);
		FOnline->ScriptSetArgInt(arg1);
		FOnline->ScriptSetArgInt(arg2);
		FOnline->ScriptRunPrepared();
	}
}

void Command_RunCheat(const char* cheatStr)
{
	ScriptString& stringCheat = ScriptString::Create(cheatStr);
	if(ASBindAndPrepare(NETWORK_SCRIPT_MODULE_CHEATS, NETWORK_SCRIPT_RUNCHEATS_NAME))
	{
		FOnline->ScriptSetArgObject(&stringCheat);
		FOnline->ScriptRunPrepared();
	}

	stringCheat.Release(); 
}
#endif

#endif