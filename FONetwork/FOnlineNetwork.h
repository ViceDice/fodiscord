

#ifndef FONETWORK_HEADER
#define FONETWORK_HEADER

#ifndef TLS
#define TLS
#define TEST_PORT "27777"
#define TEST_ADDR "127.0.0.1"
#endif


#include "Protocol.h"
#include "Network.h"

#ifdef __2238__

	#include "fonline2238.h"

	#else

	#ifdef __TLA__
	#include "fonline.h"
	#endif

#endif

#include <codecvt>
#include <string> 
#include <locale> 


#ifndef FONETWORK
extern int InitializeNetwork();
extern bool InitializeDiscordHook();
extern bool SendDiscordMessage(ScriptString* sstrIn); // angelscript dependancy
extern bool SendDiscordCommand(ScriptArray* command); // angelscript dependancy
extern void ShutdownNetwork();
extern int InitializeNetwork();
extern void ProcessDiscordCommands();
#endif



#ifdef FONETWORK
#define NETWORK_SCRIPT_MODULE_NAME "network"
#define NETWORK_SCRIPT_MODULE_CHEATS "cheats"
#define NETWORK_SCRIPT_KILL_PLAYER "void Kill_Critter(string& name)"
#define NETWORK_SCRIPT_GENERIC_RUNSCRIPT "void (int arg0, int arg1, int arg2)"
#define NETWORK_SCRIPT_RUNCHEATS_NAME	"void DiscordCheat(string& command, uint playerId)"

void Command_Kill_Player(char* name);
void Command_RunScript(const char* scriptName, const char* funcDecl, int arg0, int arg1, int arg2);
void Command_RunCheat(const char* cheatStr);
std::string GetStrArgs(char* args);
#endif

#endif