// dllmain.cpp : Defines the entry point for the DLL application.

#include "FOnlineNetwork.h"

FONLINE_DLL_ENTRY(compiler)
{
	ASEngine->RegisterGlobalFunction("bool InitializeDiscordHook()",	 asFUNCTION(InitializeDiscordHook), asCALL_CDECL);
	ASEngine->RegisterGlobalFunction("bool Discord_Message(string@)",	 asFUNCTION(SendDiscordMessage), asCALL_CDECL);
	ASEngine->RegisterGlobalFunction("bool Discord_Command(uint8[]&)",	 asFUNCTION(SendDiscordCommand), asCALL_CDECL);
	ASEngine->RegisterGlobalFunction("int InitializeNetwork()",			 asFUNCTION(InitializeNetwork), asCALL_CDECL);
	ASEngine->RegisterGlobalFunction("void ShutdownNetwork()",			 asFUNCTION(ShutdownNetwork), asCALL_CDECL);
	ASEngine->RegisterGlobalFunction("void ProcessDiscordCommands()",    asFUNCTION(ProcessDiscordCommands), asCALL_CDECL);
	InitializeNetwork();
}