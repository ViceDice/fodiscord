
#ifndef NETWORK
#define NETWORK
#include "Network.h"

void ConnectionReciever(void* data);
void ProcessResponse(char* recvbuf, unsigned int recvbuflen);
unsigned long BytesSend		= 0;
unsigned long BytesRecieved = 0;


Client_Connection :: Client_Connection()
{
	this->recieveThreadId = 0;
	this->result = NULL;
	this->ptr	 = NULL;

	this->sendbuf = "I am starting up!\n";
	this->recvbuf = new char[DEFAULT_BUFLEN];
	this->recvbuflen = DEFAULT_BUFLEN;
	this->recvbufrecv = 0;
    this->iResult = 0;
    
	this->ConnectSocket = INVALID_SOCKET;

	this->address = "";
	this->port    = "";
	this->token   = "";
	this->name    = "defaultName";

	this->status    = CONNECTION_STATUS_IDLE;
	this->processor = &::ProcessResponse; // generic response handler
}

Client_Connection :: ~Client_Connection()
{
	// cleanup
    closesocket(ConnectSocket);
    WSACleanup();
	delete this->recvbuf;
}

void Client_Connection :: SetAddress(std::string addrIn)
{
	this->address = addrIn;
}

void Client_Connection :: SetPort(std::string portIn)
{
	this->port = portIn;
}

void Client_Connection :: SetProcessor(RecieveProcessor procIn)
{
	this->processor = procIn;
}

void Client_Connection :: ProcessResponse()
{
	if(this->processor)
	{
		this->processor(this->recvbuf, this->recvbufrecv);
		return;
	}
	else
	{
		::ProcessResponse(this->recvbuf, this->recvbufrecv);
		return;
	}
}

bool Client_Connection :: Connect(char* address, char* port)
{
	this->Disconnect();
	this->SetAddress(address);
	this->SetPort(port);
	return this->Connect();
}

bool Client_Connection :: Connect()
{
	if(this->port != "" && this->address != "")
	{
		// Initialize Winsock
		iResult = WSAStartup(MAKEWORD(2,2), &wsaData);
		if (iResult != 0) 
		{
			Log(this->name.c_str());
			Log("WSAStartup failed with error: %d\n", iResult);
			return false;
		}
		ZeroMemory( &hints, sizeof(hints) );
		hints.ai_family = AF_UNSPEC;
		hints.ai_socktype = SOCK_STREAM;
		hints.ai_protocol = IPPROTO_TCP;

		iResult = getaddrinfo(address.c_str(), port.c_str(), &hints, &result);
		if ( iResult != 0 ) 
		{
			Log(this->name.c_str());
			Log("getaddrinfo failed with error: %d\n", iResult);
			WSACleanup();
			return false;
		}

		for(ptr=result; ptr != NULL ;ptr=ptr->ai_next)
		{

			// Create a SOCKET for connecting to server
			ConnectSocket = socket(ptr->ai_family, ptr->ai_socktype, 
			ptr->ai_protocol);
			if (ConnectSocket == INVALID_SOCKET) 
			{
				Log(this->name.c_str());
				Log("socket failed with error: %ld\n", WSAGetLastError());
				WSACleanup();
				return false;
			}
			  // Connect to server.
			iResult = connect( ConnectSocket, ptr->ai_addr, (int)ptr->ai_addrlen);
			if (iResult == SOCKET_ERROR) 
			{
				closesocket(ConnectSocket);
				ConnectSocket = INVALID_SOCKET;
			 continue;
			}
			break;
		}

		freeaddrinfo(result);

		if(this->token!="")
		{
			this->SetData(this->token);
		}

		SendData();

		if (ConnectSocket == INVALID_SOCKET) 
		{
			Log(this->name.c_str());
			Log("Unable to connect to server!\n");
			WSACleanup();
			return false;
		}
		this->status = CONNECTION_STATUS_IDLE;
		return true;
	}
	else
	{
		Log(this->name.c_str());
		Log("port or address are blank for connection, failed\n");
		return false;
	}
	return false;
}

int	 Client_Connection :: GetStatus()
{
	return this->status;
}

void Client_Connection :: SetToken(std::string tokenIn)
{
	this->token   = tokenIn;
}

void Client_Connection :: SetData(std::string dataIn)
{
	this->sendbuf = dataIn;
}

void Client_Connection :: SetName(std::string newName)
{
	this->name = newName;
}

std::string Client_Connection :: GetName()
{
	return this->name;
}

bool Client_Connection :: SendData()
{
	// Send an initial buffer
	const char* sendbufChars = sendbuf.c_str();
	iResult = send( ConnectSocket, sendbufChars, (int)sendbuf.length(), 0 );
	if (iResult == SOCKET_ERROR)
	{
		Log(this->name.c_str());
		Log("send failed with error: %d\n", WSAGetLastError());
		closesocket(ConnectSocket);
		WSACleanup();
		return false;
	}
	Log(this->name.c_str());
	Log("Bytes Sent: %ld\n", iResult);
	BytesSend += iResult;
	return true;

}

void Client_Connection :: StartRecieve()
{
	this->status = CONNECTION_STATUS_RECIEVING;
	this->recieveThreadId = _beginthread(&ConnectionReciever, 0, this);
}

void Client_Connection :: StopRecieve()
{
	this->status = CONNECTION_STATUS_IDLE;
	HANDLE thread = (HANDLE)this->recieveThreadId;
	WaitForSingleObject(thread, INFINITE);
	CloseHandle(thread);
}

void Client_Connection :: Recieve()
{
	 do 
	 {
		iResult = recv(ConnectSocket, recvbuf, recvbuflen, 0);
        if ( iResult > 0 )
		{
            //Log("Bytes received: %d\n", iResult);
			BytesRecieved	 += iResult;
			this->recvbufrecv = iResult;
			this->ProcessResponse();
		}
        else if ( iResult == 0 )
		{
			Log(this->name.c_str());
            Log("Connection closed\n");
			this->status = CONNECTION_STATUS_DISCONNECTED;
		}
        else
		{
			Log(this->name.c_str());
            Log("recv failed with error: %d\n", WSAGetLastError());
			this->status = CONNECTION_STATUS_DISCONNECTED;
		}

    } while( iResult > 0 );
}


bool Client_Connection :: Disconnect()
{
	// shutdown the connection since no more data will be sent
	iResult = shutdown(ConnectSocket, SD_SEND);
	if (iResult == SOCKET_ERROR) 
	{
		Log(this->name.c_str());
		Log("shutdown failed with error: %d\n", WSAGetLastError());
		closesocket(ConnectSocket);
		WSACleanup();
		return false;
	}
	this->status = CONNECTION_STATUS_DISCONNECTED;
	return true;	
}


void ConnectionReciever(void* data)
{
	Log("Connection Listener started! %d \n", GetCurrentThreadId());
	Client_Connection* connection = reinterpret_cast<Client_Connection*>(data);
	connection->Recieve();
}


void ProcessResponse(char* recvbuf, unsigned int recvbufrecv)
{
	if(recvbufrecv<=0)
		return;
	else
	{
		if(recvbufrecv==32)
		{

		}
		else
		if(recvbufrecv==16)
		{

		}
		else
		if(recvbufrecv==8)
		{

		}
		else
		if(recvbufrecv==4)
		{

		}
		else
		if(recvbufrecv==2)
		{

		}
		else
		if(recvbufrecv==1)
		{

		}
	}

}

#endif

